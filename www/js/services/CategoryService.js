angular.module('starter.controllers')
.factory('CategoryService', function($http, $window, $state) {

    //Initialiation
    var categories = [
        'Education',
        'Vacation',
        'Car',
        'Housing',
        'Others'
    ];
    return {
        //get all categories
        getCategories: function() { 
            return categories;
        }
    };
});