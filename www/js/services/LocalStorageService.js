angular.module('starter.controllers')
.factory('LocalStorageService', function($window) {
    return {
        //get localstorage base on name
        getter: function(name) { 
            return $window.localStorage[name] ? JSON.parse($window.localStorage[name]) : false;
        },
        //set localstorage base on name
        setter : function(name, data) {
            $window.localStorage[name] = JSON.stringify(data);
        }
    };
});