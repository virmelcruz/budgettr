angular.module('starter.controllers')
.factory('BudgettrService', function($http, $window, $state) {
    return{
        resetExpenses: function(){
            $window.localStorage.expenses = JSON.stringify({
                food: {
                  name: "Food",
                  value: 0,
                  id: "food",
                  icon: "ion-fork"
                },
                transpo: {
                  name: "Transportation",
                  value: 0,
                  id: "transpo",
                  icon: "ion-android-car"
                },
                rent: {
                  name: "Rent",
                  value: 0,
                  id: "rent",
                  icon: "ion-ios-home"
                },
                electricity: {
                  name: "Electricity",
                  value: 0,
                  id: "electricity",
                  icon: "ion-ios-bolt"
                },
                water: {
                  name: "Water",
                  value: 0,
                  id: "water",
                  icon: "ion-waterdrop"
                },
                comm: {
                  name: "Communication",
                  value: 0,
                  id: "comm",
                  icon: "ion-chatbox-working"
                },
                others: {
                  name: "Others",
                  value: 0,
                  id: "others",
                  icon: "ion-more"
                }


              });

              console.log($window.localStorage.expenses);
        },

        setExpenses : function(expenses){
            $window.localStorage.expenses = JSON.stringify(expenses);
        },

        getExpenses : function(){
            return JSON.parse($window.localStorage.expenses);
        },

        saveExpense: function(data){
            var expenses = this.getExpenses();
            expenses[data.id].value = data.value;
            this.setExpenses(expenses);
        }
        
    };
});