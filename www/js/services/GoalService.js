angular.module('starter.controllers')
.factory('GoalService', function($http, $window, $state) {
    return{
        getGoalDetails: function() {
            function monthDiff(d1, d2) {
                var months;
                months = (d2.getFullYear() - d1.getFullYear()) * 12;
                months -= d1.getMonth() + 1;
                months += d2.getMonth();
                return months <= 0 ? 0 : months;
            }

            
            
            var goal = {
                income: 50000,
                targetAmount : 100000,
                targetDate : new Date ("2017-04-05"),
                currentSavings: 0,
                category: "Education"
            };
            
            var monthCount = monthDiff(
                new Date(), // November 4th, 2008
                new Date(goal.targetDate)  // March 12th, 2010
            );
    
            var dayCount = Math.floor(( new Date(goal.targetDate) - new Date ) / 86400000);
            
            goal.targetPerMonth = ( goal.targetAmount - goal.currentSavings ) / monthCount;
            goal.targetPerDay = ( goal.targetAmount - goal.currentSavings ) / dayCount;
            
            return goal;
        }
        
    };
});