angular.module('starter.controllers')
.factory('AuthService', function($http, $window, $state) {
    var AuthService =  {
        login: function() {
            
        },
        checkLogin: function() {
            if($window.localStorage.isLogged) {
                $state.go("app.budgettr");
            } else {
                if($state.current.name !== 'login') {
                    $state.go("login");
                }
            }
        }
    };
    return AuthService;
});