angular.module('starter.controllers')
.controller('LoginCtrl', function($scope, $ionicModal, $timeout, AuthService, $state, $window, BudgettrService) {
    $scope.navTitle='<img class="title-image" src="/img/logo.png" />';
    
    AuthService.checkLogin();
    $scope.username = 'ben';
    $scope.password = 'password';
    
    $scope.login = function(){
        $window.localStorage.isLogged = true;
        BudgettrService.resetExpenses();
        $state.go('app.budgettr');
    };
});


