angular.module('starter.controllers')
.controller('BudgettrCtrl', function($scope, $ionicModal, $timeout, BudgettrService, $ionicPopup, LocalStorageService, GoalService, AuthService) {
	AuthService.checkLogin();
    $scope.expenses = LocalStorageService.getter('expenses') || [];
	$scope.income = 50000;
	$scope.remaining = 0;
    
    $scope.goal = LocalStorageService.getter('goal') || GoalService.getGoalDetails();
        //console.log($scope.goal)
        
    var warningShown = false;
    
	$scope.compute = function(){
	    $scope.remaining = $scope.income;
	    angular.forEach($scope.expenses, function(e){
	    	$scope.remaining -= e.value;
	    });
        
        
        if($scope.remaining < $scope.goal.targetPerMonth && !warningShown){
            var alertPopup = $ionicPopup.alert({
                title: 'Ooops!',
                template: 'Your expenses will exceed your target savings'
            });
            warningShown = true;
            alertPopup.then(function(res) {
                
            });
            
        }
	};

   

	$scope.saveSliderData = function(data){
		$scope.compute();
		BudgettrService.saveExpense(data);
	};
    
    $scope.getMaxRange = function(id){
        if($scope.expenses[id]){
            var total = 0;
            angular.forEach($scope.expenses, function(e){
                if(e.id !== id){
                    total += Number(e.value);
                    //console.log(total)
                }
            });
            return $scope.income - total;
        }
    };

});



