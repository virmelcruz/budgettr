angular.module('starter.controllers')

.controller('SavingsCtrl', function($scope, $ionicModal, $timeout, GoalService, LocalStorageService) {
    $scope.goal = LocalStorageService.getter('goal') || GoalService.getGoalDetails();
    
    $scope.saving = {
        duration : 'm',
        amount : 0,
        maturityDate: null
    };
    
    $scope.errmsg = '';
    $scope.calculate = function(){
        $scope.errmsg = '';
        if($scope.saving.amount > $scope.goal.targetAmount){
            $scope.saving.maturityDate = null;
            $scope.errmsg = 'Saving per month cannot be greater that your target savings';
            return;
        }
        
        if($scope.saving.duration === 'm'){
            var remaining = $scope.goal.targetAmount - $scope.goal.currentSavings;
            
            var count = Math.floor(remaining / $scope.saving.amount );
            var date = new Date();
            $scope.saving.maturityDate = new Date(date.getFullYear(), date.getMonth() + count, date.getDate(), 0, 0, 0);
        } else{
            var remaining = $scope.goal.targetAmount - $scope.goal.currentSavings;
            
            var count = Math.floor(remaining / $scope.saving.amount );
            var date = new Date();
            $scope.saving.maturityDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + count, 0, 0, 0);
        }
    };

    
});