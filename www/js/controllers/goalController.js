angular.module('starter.controllers')
.controller('GoalCtrl', function($scope, $window, $state,
    GoalService, CategoryService, LocalStorageService) {
	
	//Initialization of Categories
    $scope.categories = CategoryService.getCategories();

    //priority getter > goalDetails('correct me if im wrong')
    $scope.goal = LocalStorageService.getter('goal') || GoalService.getGoalDetails();

    //form submit handler
    $scope.formSubmit = function(formData) {
    	//save data to localstorage
    	LocalStorageService.setter('goal', formData);
        $state.go('app.budgettr');
    };
    
});