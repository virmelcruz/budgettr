angular.module('starter.controllers', [])
.controller('AppCtrl', function($scope, $ionicModal, $ionicHistory, $window, $state, AuthService) {
    $scope.navTitle = '<img class="title-image" src="../img/logo.png" />';
    
    AuthService.checkLogin();
    $scope.logout = function() {
        $window.localStorage.clear();
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        $state.go('login');
    };
});